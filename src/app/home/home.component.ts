import { Component, OnInit } from '@angular/core';
import { MessageModal } from '../message.modal';
import { map } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { MessageServices } from '../message.services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  message: MessageModal[] | null = null;
  preloader = false

  constructor(private http: HttpClient,
              private route: ActivatedRoute,
              private service: MessageServices) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params)=>{
     return this.getMessage();
    })
  };

  getMessage(){
    this.preloader = true;
    this.http.get<{[id: string]: MessageModal}>('https://mlacvit-10af9-default-rtdb.firebaseio.com/messanger/message.json')
      .pipe(map(result => {
        if (result === null || undefined){
          return [];
        }
        return  Object.keys(result).map(id => {
          const data = result[id];
          return new MessageModal(
            id,
            data.title,
            data.text,
            data.date,
          );
        });
      }))
      .subscribe(mess => {
        this.message = mess;
        this.preloader = false;
      });
  }

  addToId(id: string) {
    this.service.messId = id;
  }
}
