export class MessageModal {
  constructor(
    public id: string,
    public title: string,
    public text: string,
    public date: string,
  ) {}
}
