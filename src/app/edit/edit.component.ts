import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { MessageModal } from '../message.modal';
import { map } from 'rxjs';
import { MessageServices } from '../message.services';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  title = '';
  text = '';
  date = '';
  result!: MessageModal;
  constructor(private http: HttpClient, private route: ActivatedRoute, private services: MessageServices) { }

  ngOnInit(): void {

    this.route.params.subscribe(()=> {
      this.addOneMessage()
    })
  };

  addOneMessage(){
    this.http.get<MessageModal>(`https://mlacvit-10af9-default-rtdb.firebaseio.com/messanger/message/${this.services.messId}.json`)
      .pipe(map(result => {
        return new MessageModal(
          result.id,
          result.title,
          result.text,
          result.date,
        );
      }))
      .subscribe(mess => {
        this.result = mess;
        this.title = this.result.title;
        this.text = this.result.text;
        this.date = this.result.date;
      });
  }

  renameMessage(){
    const date = this.date;
    const title = this.title;
    const text = this.text;
    const body = {date, text, title};
    return this.http.put(`https://mlacvit-10af9-default-rtdb.firebaseio.com/messanger/message/${this.services.messId}.json`, body).subscribe();
  }
}
