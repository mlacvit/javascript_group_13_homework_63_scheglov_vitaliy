import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewMessageComponent } from './new-message/new-message.component';
import { EditComponent } from './edit/edit.component';
import { MessageComponent } from './message/message.component';
import { HomeComponent } from './home/home.component';
import { ContactsComponent } from './contacts/contacts.component';
import {  HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MessageItemComponent } from './message/message-item/message-item.component';
import { AboutComponent } from './about/about.component';
import { MessageServices } from './message.services';

@NgModule({
  declarations: [
    AppComponent,
    NewMessageComponent,
    EditComponent,
    MessageComponent,
    HomeComponent,
    ContactsComponent,
    MessageItemComponent,
    AboutComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [MessageServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
