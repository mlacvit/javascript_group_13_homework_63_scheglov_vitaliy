import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
import { ContactsComponent } from './contacts/contacts.component';
import { MessageComponent } from './message/message.component';
import { NewMessageComponent } from './new-message/new-message.component';
import { MessageItemComponent } from './message/message-item/message-item.component';
import { AboutComponent } from './about/about.component';

const routes: Routes = [
  {path: '', component: HomeComponent, children: [
      {path: 'edit', component: EditComponent},
      {path: 'contact', component: ContactsComponent},
      {path: 'about', component: AboutComponent},
      {path: 'add', component: NewMessageComponent},
      {path: ':id', component: MessageComponent},
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
