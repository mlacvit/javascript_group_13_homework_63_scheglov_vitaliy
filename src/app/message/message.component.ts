import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageModal } from '../message.modal';
import { map } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { MessageServices } from '../message.services';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  messageId = '';
  result!: MessageModal;

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params)=>{
      this.messageId = params['id'];
    })

    this.route.params.subscribe(()=> {
      this.addOneMessage()
    })
  };

  addOneMessage(){
    this.http.get<MessageModal>(`https://mlacvit-10af9-default-rtdb.firebaseio.com/messanger/message/${this.messageId}.json`)
      .pipe(map(result => {
        return new MessageModal(
          this.messageId,
          result.title,
          result.text,
          result.date,
        );
      }))
      .subscribe(mess => {
        this.result = mess;
      });
  }

  deleteMess(id: string) {
    this.http.delete(`https://mlacvit-10af9-default-rtdb.firebaseio.com/messanger/message/${id}.json`).subscribe();

  }

}
