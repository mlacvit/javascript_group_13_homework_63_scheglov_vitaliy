import { Component, Input, OnInit } from '@angular/core';
import { MessageModal } from '../../message.modal';

@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.css']
})
export class MessageItemComponent implements OnInit {
  @Input() message!: MessageModal;
  constructor() { }

  ngOnInit(): void {
  }

}
