import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css']
})
export class NewMessageComponent implements OnInit {
  title = '';
  text = '';
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  sendMessage(){
    const date = new Date()
    const title = this.title;
    const text = this.text;
    const body = {title, text, date};
    this.http.post('https://mlacvit-10af9-default-rtdb.firebaseio.com/messanger/message.json', body).subscribe()
  }

}
